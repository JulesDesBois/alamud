from .action import Action2
from mud.events import BoireEvent

class BoireAction(Action2):
    EVENT = BoireEvent
    ACTION = "boire"
    RESOLVE_OBJECT = "resolve_for_use"
