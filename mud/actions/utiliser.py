# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import UtiliserEvent

class UtiliserAction(Action2):
    EVENT = UtiliserEvent
    ACTION = "utiliser"
    RESOLVE_OBJECT = "resolve_for_use"
